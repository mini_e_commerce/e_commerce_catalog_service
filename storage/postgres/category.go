package postgres

import (
	"context"
	"database/sql"
	"mini_e_commerce/e_commerce_catalog_service/genproto/catalog_service"

	"github.com/google/uuid"
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4/pgxpool"

	"mini_e_commerce/e_commerce_catalog_service/pkg/helper"
	"mini_e_commerce/e_commerce_catalog_service/storage"
)

type CategoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) storage.CategoryRepoI {
	return &CategoryRepo{
		db: db,
	}
}

func (c *CategoryRepo) Create(ctx context.Context, req *catalog_service.CreateCategory) (resp *catalog_service.CategoryPrimaryKey, err error) {

	var id = uuid.New().String()

	query := `INSERT INTO "category" (
				id,
				name,
				updated_at
			) VALUES ($1, $2, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.Name,
	)

	if err != nil {
		return nil, err
	}

	return &catalog_service.CategoryPrimaryKey{Id: id}, nil
}

func (c *CategoryRepo) GetByPKey(ctx context.Context, req *catalog_service.CategoryPrimaryKey) (resp *catalog_service.Category, err error) {

	query := `
			SELECT
				c.id,
				c.name,
				c.created_at,
				c.updated_at,
				JSONB_AGG(JSON_BUILD_OBJECT(
					'id',         p.id,
					'name',       p.name,
					'description',p.description,
					'price',      p.price,
					'quantity',   p.quantity
				)) as products
			FROM "category" as c
			LEFT JOIN product as p ON p.category_id = c.id
			WHERE c.id = $1
			GROUP BY c.id
	`

	var (
		id         sql.NullString
		name       sql.NullString
		createdAt  sql.NullString
		updatedAt  sql.NullString
		productObj pgtype.JSONB
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&createdAt,
		&updatedAt,
		&productObj,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.Category{
		Id:        id.String,
		Name:      name.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	productObj.AssignTo(&resp.Products)

	return
}

func (c *CategoryRepo) GetAll(ctx context.Context, req *catalog_service.GetListCategoryRequest) (resp *catalog_service.GetListCategoryResponse, err error) {

	resp = &catalog_service.GetListCategoryResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "category"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Categorys = append(resp.Categorys, &catalog_service.Category{
			Id:        id.String,
			Name:      name.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *CategoryRepo) Update(ctx context.Context, req *catalog_service.UpdateCategory) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
				"category"
			SET
				name = :name,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":   req.GetId(),
		"name": req.GetName(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *CategoryRepo) Delete(ctx context.Context, req *catalog_service.CategoryPrimaryKey) error {

	query := `DELETE FROM "category" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
