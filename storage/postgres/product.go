package postgres

import (
	"context"
	"database/sql"
	"mini_e_commerce/e_commerce_catalog_service/genproto/catalog_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"mini_e_commerce/e_commerce_catalog_service/pkg/helper"
	"mini_e_commerce/e_commerce_catalog_service/storage"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) storage.ProductRepoI {
	return &ProductRepo{
		db: db,
	}
}

func (c *ProductRepo) Create(ctx context.Context, req *catalog_service.CreateProduct) (resp *catalog_service.ProductPrimaryKey, err error) {

	var id = uuid.New().String()

	query := `INSERT INTO "product" (
				id,
				name,
				category_id,
				description,
				price,
				quantity,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.Name,
		req.CategoryId,
		req.Description,
		req.Price,
		req.Quantity,
	)

	if err != nil {
		return nil, err
	}

	return &catalog_service.ProductPrimaryKey{Id: id}, nil
}

func (c *ProductRepo) GetByPKey(ctx context.Context, req *catalog_service.ProductPrimaryKey) (resp *catalog_service.Product, err error) {

	query := `
		SELECT
			id,
			name,
			category_id,
			description,
			price,
			quantity,
			created_at,
			updated_at
		FROM "product"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		name        sql.NullString
		categoryId  sql.NullString
		description sql.NullString
		price       sql.NullFloat64
		quantity    sql.NullInt64
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&categoryId,
		&description,
		&price,
		&quantity,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.Product{
		Id:          id.String,
		Name:        name.String,
		CategoryId:  categoryId.String,
		Description: description.String,
		Price:       float32(price.Float64),
		Quantity:    quantity.Int64,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *ProductRepo) GetAll(ctx context.Context, req *catalog_service.GetListProductRequest) (resp *catalog_service.GetListProductResponse, err error) {

	resp = &catalog_service.GetListProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			category_id,
			description,
			price,
			quantity,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			categoryId  sql.NullString
			description sql.NullString
			price       sql.NullFloat64
			quantity    sql.NullInt64
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&categoryId,
			&description,
			&price,
			&quantity,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Products = append(resp.Products, &catalog_service.Product{
			Id:          id.String,
			Name:        name.String,
			Description: description.String,
			Price:       float32(price.Float64),
			Quantity:    quantity.Int64,
			CategoryId:  categoryId.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *ProductRepo) Update(ctx context.Context, req *catalog_service.UpdateProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
				"product"
			SET
				name = :name,
				category_id = :category_id,
				description = : description,
				price = : price,
				quantity = : quantity,	
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":   req.GetId(),
		"name": req.GetName(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ProductRepo) Delete(ctx context.Context, req *catalog_service.ProductPrimaryKey) error {

	query := `DELETE FROM "product" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
