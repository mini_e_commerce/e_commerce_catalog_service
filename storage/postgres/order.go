package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"mini_e_commerce/e_commerce_catalog_service/genproto/catalog_service"
	"strings"

	"github.com/google/uuid"
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"mini_e_commerce/e_commerce_catalog_service/storage"
)

type OrderRepo struct {
	db *pgxpool.Pool
}

func NewOrderRepo(db *pgxpool.Pool) storage.OrderRepoI {
	return &OrderRepo{
		db: db,
	}
}

func (c *OrderRepo) Create(ctx context.Context, req *catalog_service.CreateOrder) (resp *catalog_service.OrderPrimaryKey, err error) {

	var order_id = uuid.New().String()
	var arr []string
	tx, err := c.db.BeginTx(ctx, pgx.TxOptions{})
	defer func() {
		if err != nil {
			tx.Rollback(ctx)
		}
		tx.Commit(ctx)
	}()

	query := `INSERT INTO "order" (
				id,
				client_id,
				price,
				updated_at
			) VALUES ($1, $2, $3, now())
		`

	_, err = tx.Exec(ctx,
		query,
		order_id,
		req.ClientId,
		req.Price,
	)

	if err != nil {

		return nil, err
	}

	arr = strings.Split(req.ProductIds, ", ")

	queryCO := `
			INSERT INTO "order_products" (
				id,
				order_id,
				product_id
			) VALUES
		`

	for _, product_id := range arr {

		var idCO = uuid.New().String()

		queryCO += fmt.Sprintf("('%s', '%s', '%s'),", idCO, order_id, product_id)
	}

	queryCO = queryCO[:len(queryCO)-1]

	_, err = tx.Exec(ctx, queryCO)

	if err != nil {

		return nil, err
	}

	return &catalog_service.OrderPrimaryKey{Id: order_id}, nil
}

func (c *OrderRepo) GetByPKey(ctx context.Context, req *catalog_service.OrderPrimaryKey) (resp *catalog_service.OrderProduct, err error) {

	query := `
		SELECT
			id,
			client_id,
			price,
			status,
			created_at,
			updated_at
		FROM "order"
		WHERE id = $1
	`

	query2 := `
		SELECT 
			p.name,
			p.price,
			p.description,
			p.category_id
		FROM 
			"order_products" as op
		JOIN "product" as p ON op.product_id = p.id
		WHERE op.order_id = $1
	`

	var (
		id        sql.NullString
		client_id sql.NullString
		price     sql.NullFloat64
		status    sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&client_id,
		&price,
		&status,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.OrderProduct{
		Id:        id.String,
		ClientId:  client_id.String,
		Price:     float32(price.Float64),
		Status:    status.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	rows, err := c.db.Query(ctx, query2, req.Id)

	for rows.Next() {
		var (
			pName        sql.NullString
			pPrice       sql.NullFloat64
			pDescription sql.NullString
			pCategoryId  sql.NullString
		)

		rows.Scan(
			&pName,
			&pPrice,
			&pDescription,
			&pCategoryId,
		)

		resp.Products = append(resp.Products, &catalog_service.OrderP{
			Name:        pName.String,
			Price:       float32(pPrice.Float64),
			Description: pDescription.String,
			CategoryId:  pCategoryId.String,
		})
	}

	return
}

func (c *OrderRepo) GetByClientPKey(ctx context.Context, req *catalog_service.OrderPrimaryKey) (resp *catalog_service.OrderClient, err error) {

	var objectorder pgtype.JSONB
	var FirstName sql.NullString

	query := `
		SELECT
			c.first_name,
			JSONB_AGG(JSON_BUILD_OBJECT(
				'id', o.id,
				'price', o.price,
				'status', o.status,
				'created_at', o.created_at,
				'updated_at', o.updated_at
			)) as orders
		FROM "order" as o
		JOIN "client" as c ON c.id = o.client_id
		WHERE o.client_id = $1
		GROUP BY c.first_name
	`

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&FirstName,
		&objectorder,
	)
	if err != nil {
		return resp, err
	}

	resp = &catalog_service.OrderClient{
		FirstName: FirstName.String,
	}

	objectorder.AssignTo(&resp.ClientOrder)

	return
}

// func (c *OrderRepo) GetAll(ctx context.Context, req *catalog_service.GetListOrderRequest) (resp *catalog_service.GetListOrderResponse, err error) {

// 	resp = &catalog_service.GetListOrderResponse{}

// 	var (
// 		query  string
// 		limit  = ""
// 		offset = " OFFSET 0 "
// 		params = make(map[string]interface{})
// 		filter = " WHERE TRUE"
// 		sort   = " ORDER BY created_at DESC"
// 	)

// 	query = `
// 		SELECT
// 			COUNT(*) OVER(),
// 			id,
// 			name,
// 			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
// 			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
// 		FROM "Order"
// 	`

// 	if req.GetLimit() > 0 {
// 		limit = " LIMIT :limit"
// 		params["limit"] = req.Limit
// 	}

// 	if req.GetOffset() > 0 {
// 		offset = " OFFSET :offset"
// 		params["offset"] = req.Offset
// 	}

// 	query += filter + sort + offset + limit

// 	query, args := helper.ReplaceQueryParams(query, params)
// 	rows, err := c.db.Query(ctx, query, args...)
// 	defer rows.Close()

// 	if err != nil {
// 		return resp, err
// 	}

// 	for rows.Next() {
// 		var (
// 			id        sql.NullString
// 			name      sql.NullString
// 			createdAt sql.NullString
// 			updatedAt sql.NullString
// 		)

// 		err := rows.Scan(
// 			&resp.Count,
// 			&id,
// 			&name,
// 			&createdAt,
// 			&updatedAt,
// 		)

// 		if err != nil {
// 			return resp, err
// 		}

// 		resp.Orders = append(resp.Orders, &catalog_service.Order{
// 			Id:        id.String,
// 			Name:      name.String,
// 			CreatedAt: createdAt.String,
// 			UpdatedAt: updatedAt.String,
// 		})
// 	}

// 	return
// }

// func (c *OrderRepo) Update(ctx context.Context, req *catalog_service.UpdateOrder) (rowsAffected int64, err error) {

// 	var (
// 		query  string
// 		params map[string]interface{}
// 	)

// 	query = `
// 			UPDATE
// 				"Order"
// 			SET
// 				name = :name,
// 				updated_at = now()
// 			WHERE
// 				id = :id`
// 	params = map[string]interface{}{
// 		"id":   req.GetId(),
// 		"name": req.GetName(),
// 	}

// 	query, args := helper.ReplaceQueryParams(query, params)

// 	result, err := c.db.Exec(ctx, query, args...)
// 	if err != nil {
// 		return
// 	}

// 	return result.RowsAffected(), nil
// }

func (c *OrderRepo) Delete(ctx context.Context, req *catalog_service.OrderPrimaryKey) error {

	queryOrderProducts := `
		DELETE FROM "order_products" WHERE order_id = $1
	`
	_, err := c.db.Exec(ctx, queryOrderProducts, req.Id)

	if err != nil {
		return err
	}

	query := `DELETE FROM "order" WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
