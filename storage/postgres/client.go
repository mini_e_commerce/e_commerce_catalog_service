package postgres

import (
	"context"
	"database/sql"
	"mini_e_commerce/e_commerce_catalog_service/genproto/catalog_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"mini_e_commerce/e_commerce_catalog_service/pkg/helper"
	"mini_e_commerce/e_commerce_catalog_service/storage"
)

type ClientRepo struct {
	db *pgxpool.Pool
}

func NewClientRepo(db *pgxpool.Pool) storage.ClientRepoI {
	return &ClientRepo{
		db: db,
	}
}

func (c *ClientRepo) Create(ctx context.Context, req *catalog_service.CreateClient) (resp *catalog_service.ClientPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "client" (
				id,
				first_name,
				last_name,
				phone_number,
				updated_at
			) VALUES ($1, $2, $3, $4, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
	)

	if err != nil {
		return nil, err
	}

	return &catalog_service.ClientPrimaryKey{Id: id.String()}, nil
}

func (c *ClientRepo) GetByPKey(ctx context.Context, req *catalog_service.ClientPrimaryKey) (resp *catalog_service.Client, err error) {

	query := `
		SELECT
			id,
			first_name,
			last_name,
			phone_number,
			created_at,
			updated_at
		FROM "client"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		firstName   sql.NullString
		lastName    sql.NullString
		phoneNumber sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&firstName,
		&lastName,
		&phoneNumber,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.Client{
		Id:          id.String,
		FirstName:   firstName.String,
		LastName:    lastName.String,
		PhoneNumber: phoneNumber.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *ClientRepo) GetAll(ctx context.Context, req *catalog_service.GetListClientRequest) (resp *catalog_service.GetListClientResponse, err error) {

	resp = &catalog_service.GetListClientResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name,
			last_name,
			phone_number,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "client"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			firstName   sql.NullString
			lastName    sql.NullString
			phoneNumber sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&firstName,
			&lastName,
			&phoneNumber,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Clients = append(resp.Clients, &catalog_service.Client{
			Id:          id.String,
			FirstName:   firstName.String,
			LastName:    lastName.String,
			PhoneNumber: phoneNumber.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *ClientRepo) Update(ctx context.Context, req *catalog_service.UpdateClient) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "client"
			SET
				first_name = :first_name,
				last_name = :last_name,
				type = :type,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"first_name":   req.GetFirstName(),
		"last_name":    req.GetLastName(),
		"phone_number": req.GetPhoneNumber(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ClientRepo) Delete(ctx context.Context, req *catalog_service.ClientPrimaryKey) error {

	query := `DELETE FROM "client" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
