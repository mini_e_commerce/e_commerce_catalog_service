package storage

import (
	"context"
	"mini_e_commerce/e_commerce_catalog_service/genproto/catalog_service"
)

type StorageI interface {
	CloseDB()
	Client() ClientRepoI
	Category() CategoryRepoI
	Order() OrderRepoI
	Product() ProductRepoI
}

type ClientRepoI interface {
	Create(ctx context.Context, req *catalog_service.CreateClient) (resp *catalog_service.ClientPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.ClientPrimaryKey) (resp *catalog_service.Client, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListClientRequest) (resp *catalog_service.GetListClientResponse, err error)
	Update(ctx context.Context, req *catalog_service.UpdateClient) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *catalog_service.ClientPrimaryKey) error
}

type CategoryRepoI interface {
	Create(ctx context.Context, req *catalog_service.CreateCategory) (resp *catalog_service.CategoryPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.CategoryPrimaryKey) (resp *catalog_service.Category, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListCategoryRequest) (resp *catalog_service.GetListCategoryResponse, err error)
	Update(ctx context.Context, req *catalog_service.UpdateCategory) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *catalog_service.CategoryPrimaryKey) error
}

type OrderRepoI interface {
	Create(ctx context.Context, req *catalog_service.CreateOrder) (resp *catalog_service.OrderPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.OrderPrimaryKey) (resp *catalog_service.OrderProduct, err error)
	GetByClientPKey(ctx context.Context, req *catalog_service.OrderPrimaryKey) (resp *catalog_service.OrderClient, err error)
	// GetAll(ctx context.Context, req *catalog_service.GetListOrderRequest) (resp *catalog_service.GetListOrderResponse, err error)
	// Update(ctx context.Context, req *catalog_service.UpdateOrder) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *catalog_service.OrderPrimaryKey) error
}

type ProductRepoI interface {
	Create(ctx context.Context, req *catalog_service.CreateProduct) (resp *catalog_service.ProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.ProductPrimaryKey) (resp *catalog_service.Product, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListProductRequest) (resp *catalog_service.GetListProductResponse, err error)
	Update(ctx context.Context, req *catalog_service.UpdateProduct) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *catalog_service.ProductPrimaryKey) error
}
