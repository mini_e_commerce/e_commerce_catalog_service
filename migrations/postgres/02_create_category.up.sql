
CREATE TABLE "category" (
  "id" uuid PRIMARY KEY,
  "name" varchar not null,
  "created_at" timestamp default current_timestamp not null,
  "updated_at" timestamp
);