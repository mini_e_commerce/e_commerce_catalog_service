package service

import (
	"context"
	"mini_e_commerce/e_commerce_catalog_service/config"
	"mini_e_commerce/e_commerce_catalog_service/genproto/catalog_service"
	"mini_e_commerce/e_commerce_catalog_service/grpc/client"
	"mini_e_commerce/e_commerce_catalog_service/pkg/logger"
	"mini_e_commerce/e_commerce_catalog_service/storage"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type OrderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*catalog_service.UnimplementedOrderServiceServer
}

func NewOrderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *OrderService {
	return &OrderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *OrderService) Create(ctx context.Context, req *catalog_service.CreateOrder) (resp *catalog_service.OrderProduct, err error) {

	i.log.Info("---CreateOrder------>", logger.Any("req", req))

	pKey, err := i.strg.Order().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateOrder->Order->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Order().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) GetByID(ctx context.Context, req *catalog_service.OrderPrimaryKey) (resp *catalog_service.OrderProduct, err error) {

	i.log.Info("---GetUserByID------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetUserByID->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) GetByClientID(ctx context.Context, req *catalog_service.OrderPrimaryKey) (resp *catalog_service.OrderClient, err error) {

	i.log.Info("---GetClientByID------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetByClientPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetClientByID->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

// func (i *OrderService) GetList(ctx context.Context, req *catalog_service.GetListOrderRequest) (resp *catalog_service.GetListOrderResponse, err error) {

// 	i.log.Info("---GetUsers------>", logger.Any("req", req))

// 	resp, err = i.strg.Order().GetAll(ctx, req)
// 	if err != nil {
// 		i.log.Error("!!!GetUsers->Order->Get--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	return
// }

// func (i *OrderService) Update(ctx context.Context, req *catalog_service.UpdateOrder) (resp *catalog_service.Order, err error) {

// 	i.log.Info("---UpdateOrder------>", logger.Any("req", req))

// 	rowsAffected, err := i.strg.Order().Update(ctx, req)

// 	if err != nil {
// 		i.log.Error("!!!UpdateOrder--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	if rowsAffected <= 0 {
// 		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
// 	}

// 	resp, err = i.strg.Order().GetByPKey(ctx, &catalog_service.OrderPrimaryKey{Id: req.Id})
// 	if err != nil {
// 		i.log.Error("!!!GetOrder->Order->Get--->", logger.Error(err))
// 		return nil, status.Error(codes.NotFound, err.Error())
// 	}

// 	return resp, err
// }

func (i *OrderService) Delete(ctx context.Context, req *catalog_service.OrderPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteOrder------>", logger.Any("req", req))

	err = i.strg.Order().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
