package service

import (
	"context"
	"mini_e_commerce/e_commerce_catalog_service/genproto/catalog_service"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"mini_e_commerce/e_commerce_catalog_service/config"
	"mini_e_commerce/e_commerce_catalog_service/grpc/client"
	"mini_e_commerce/e_commerce_catalog_service/pkg/logger"
	"mini_e_commerce/e_commerce_catalog_service/storage"
)

type ClientService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*catalog_service.UnimplementedClientServiceServer
}

func NewClientService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ClientService {
	return &ClientService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ClientService) Create(ctx context.Context, req *catalog_service.CreateClient) (resp *catalog_service.Client, err error) {

	i.log.Info("---CreateClient------>", logger.Any("req", req))

	pKey, err := i.strg.Client().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateClient->Client->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Client().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyClient->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ClientService) GetByID(ctx context.Context, req *catalog_service.ClientPrimaryKey) (resp *catalog_service.Client, err error) {

	i.log.Info("---GetClientByID------>", logger.Any("req", req))

	resp, err = i.strg.Client().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetClientByID->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ClientService) GetList(ctx context.Context, req *catalog_service.GetListClientRequest) (resp *catalog_service.GetListClientResponse, err error) {

	i.log.Info("---GetClients------>", logger.Any("req", req))

	resp, err = i.strg.Client().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetClients->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ClientService) Update(ctx context.Context, req *catalog_service.UpdateClient) (resp *catalog_service.Client, err error) {

	i.log.Info("---UpdateClient------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Client().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateClient--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Client().GetByPKey(ctx, &catalog_service.ClientPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetClient->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ClientService) Delete(ctx context.Context, req *catalog_service.ClientPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteClient------>", logger.Any("req", req))

	err = i.strg.Client().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteClient->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
