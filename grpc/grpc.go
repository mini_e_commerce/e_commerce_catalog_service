package grpc

import (
	"mini_e_commerce/e_commerce_catalog_service/genproto/catalog_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"mini_e_commerce/e_commerce_catalog_service/config"
	"mini_e_commerce/e_commerce_catalog_service/grpc/client"
	"mini_e_commerce/e_commerce_catalog_service/grpc/service"
	"mini_e_commerce/e_commerce_catalog_service/pkg/logger"
	"mini_e_commerce/e_commerce_catalog_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	catalog_service.RegisterClientServiceServer(grpcServer, service.NewClientService(cfg, log, strg, srvc))
	catalog_service.RegisterCatgoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))
	catalog_service.RegisterProductServiceServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))
	catalog_service.RegisterOrderServiceServer(grpcServer, service.NewOrderService(cfg, log, strg, srvc))
	reflection.Register(grpcServer)
	return
}
